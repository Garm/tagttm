const { app, BrowserWindow } = require('electron')
const { is } = require('electron-util')

let win

function createWindow() {
    win = new BrowserWindow()

    win.loadFile('index.html')
    win.setTitle("The Artist's Guide to Time Management")

    if (is.development) {
        win.webContents.openDevTools()
    }

    win.on('closed', () => {
        win = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (!is.macos) {
        app.quit()
    }
})

app.on('activate', () => {
    if(win === null) {
        createWindow()
    }
})